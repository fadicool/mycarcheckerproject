# new feature
# Tags: optional

Feature: A description
  As a user of the Car-checker website
  I want to be able to enter a vehicle registration number
  So I can check the vehicles details ie Registration, make, model, color and year.

  @Test-1
  Scenario Outline: The user enters a vehicle registration number
    Given I am on the car checkers search Page
    When I search registration index <regIndex> for specific vehicles
    Then I should be able to validate the displayed <regIndex> vehicles details
    Examples:
      | regIndex |
      | 0        |
      | 1        |
      | 2        |
      | 3        |

