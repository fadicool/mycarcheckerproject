package org.carchecker.stepDefs;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.carchecker.pages.ValidationPage;
import org.junit.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.carchecker.pages.PageManager;
import org.carchecker.pages.SearchPage;

public class searchStepdefs {

    private PageManager pageMg;
    private SearchPage searchPage;
    private ValidationPage validationPage;

    public searchStepdefs(PageManager pgMg) {
        this.pageMg = pgMg;
    }

    @Given("^I am on the car checkers search Page$")
    public void i_am_on_the_car_checkers_search_Page() throws Throwable {
        searchPage = pageMg.getBasePage().goToSearchPage();
    }

    @When("^I search registration index (\\d+) for specific vehicles$")
    public void i_search_registration_index_for_specific_vehicles(int arg1) throws Throwable {
        validationPage = searchPage.checkRegNumber(arg1);
    }

    @Then("^I should be able to validate the displayed (\\d+) vehicles details$")
    public void i_should_be_able_to_validate_the_displayed_vehicles_details(int arg1) throws Throwable {
        List<String> webPageResultList = validationPage.getWebPageResultingData();
        List<String> outputFileRecords = validationPage.getOutputFileRecords();

//        assert(webPageResultList.equals(outputFileRecords));

        System.out.println("--- Webpage data Results: " + webPageResultList);
        System.out.println("--- Output File Results: " + outputFileRecords.get(arg1));
        System.out.println("");

    }


}


