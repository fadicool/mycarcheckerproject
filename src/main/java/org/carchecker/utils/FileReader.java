package org.carchecker.utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class FileReader {

    private final static List<String> vehicleRegistrationList = new ArrayList<>();

    private final String registrationRegex = "([A-Z]{2}[0-9]{2}[\\s*][A-Z]{3})|([A-Z]{2}[0-9]{2}[A-Z]{3})";
    private final Pattern pattern = Pattern.compile(registrationRegex, Pattern.MULTILINE);

    public List<String> getVehicleReg() throws IOException {
        // readFile() Returns the car_input.txt file as a String object
        String responseData = inputFileReader();
        // pattern.matcher() parses through the reposonceData String and extracts any matching Regex
        final Matcher matcher = pattern.matcher(responseData);
        while (matcher.find()) {
            vehicleRegistrationList.add(matcher.group(0).replaceAll("\\s+", ""));
        }
        return vehicleRegistrationList;
    }

    public String inputFileReader() throws IOException {
        String propFile = "car_input.txt";
        String filename = System.getProperty("user.dir") + "/src/test/resources/data/" + propFile;
        Path pathToFile = Paths.get(filename);
        return new String(Files.readAllBytes(Paths.get(pathToFile.toAbsolutePath().toString())), StandardCharsets.UTF_8);
    }

    private static String outputFileReader() throws IOException {
        String propFile = "car_output.txt";
        String filename = System.getProperty("user.dir") + "/src/test/resources/data/" + propFile;
        Path pathToFile = Paths.get(filename);
        return new String(Files.readAllBytes(Paths.get(pathToFile.toAbsolutePath().toString())), StandardCharsets.UTF_8);
    }

    public static List<String> extractValues() throws IOException {
        String[] outputData = outputFileReader().split("\n");
        String[] outputArrays = Arrays.copyOfRange(outputData, 1, outputData.length);

        return Arrays.asList(outputArrays);

    }


}
