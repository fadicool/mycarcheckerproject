package org.carchecker.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.carchecker.driver.DriverFactory;


public class BasePage {

    protected WebDriver driver;

    public BasePage(WebDriver driver) {
        this.driver = driver;
    }

    public SearchPage goToSearchPage(){
        String baseURL = "https://cartaxcheck.co.uk/";
        driver.navigate().to(baseURL);
//        driver.get(baseURL);
        return PageFactory.initElements(driver, SearchPage.class);
    }

}




