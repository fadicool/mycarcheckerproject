package org.carchecker.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.carchecker.driver.DriverFactory;
import org.carchecker.pages.*;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Johannes Adu on 11/8/2018.
 */
public class PageManager {

    private static BasePage basePage;
    private static SearchPage searchPage;
    private static ValidationPage validationPage;

    private static final WebDriver driver = DriverFactory.createDriver();

    public BasePage getBasePage() {
        if(basePage != null) return basePage;
        basePage = PageFactory.initElements(driver, BasePage.class);
        return basePage;
    }

    public SearchPage getSearchPage() {
        if(searchPage != null) return searchPage;
        searchPage = PageFactory.initElements(driver, SearchPage.class);
        return searchPage;
    }

    public ValidationPage getValidationPage() {
        if(validationPage != null) return validationPage;
        validationPage = PageFactory.initElements(driver, ValidationPage.class);
        return validationPage;
    }
}
