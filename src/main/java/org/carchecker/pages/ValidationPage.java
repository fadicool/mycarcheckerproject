package org.carchecker.pages;

import org.carchecker.utils.FileReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ValidationPage extends BasePage {

    @FindBy(id = "m")
    private WebElement carDetails = null;

    public ValidationPage(WebDriver driver) {
        super(driver);
    }


    public List<String> getWebPageResultingData() throws InterruptedException {
        Thread.sleep(1000);
        List<String> webPageResultingData = new ArrayList<>();
        int numberOfItems = 1;

        for (WebElement element : carDetails.findElements(By.cssSelector("div[class*='m-w-']")).get(3).findElements(By.tagName("dd"))){
            if(element.getText() != null && numberOfItems <=5) webPageResultingData.add(element.getText());
            numberOfItems ++;
        }
        return webPageResultingData;
    }


    public List<String> getOutputFileRecords() throws IOException {
        FileReader fReader = new FileReader();

        return FileReader.extractValues();
    }


}
