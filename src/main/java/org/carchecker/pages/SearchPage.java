package org.carchecker.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.io.IOException;
import java.util.List;
import org.carchecker.utils.FileReader;


public class SearchPage extends BasePage{

    @FindBy(id="vrm-input")
    private WebElement vrmInput = null;

    @FindBy(css="form > button")
    private WebElement freeCarCheckButton = null;


    public SearchPage(WebDriver driver) {
        super(driver);
    }

    public ValidationPage checkRegNumber(int indx) throws IOException {
        FileReader fReader = new FileReader();
        List<String> vehicleRegistrationList = fReader.getVehicleReg();
        String selectedVehiclesReg = vehicleRegistrationList.get(indx);
        System.out.println("--- LIST of Extracted Vehicle Registration Numbers: " + vehicleRegistrationList);

        vrmInput.sendKeys(selectedVehiclesReg);
        freeCarCheckButton.click();

        vehicleRegistrationList.clear();
        return PageFactory.initElements(driver, ValidationPage.class);

    }


}
